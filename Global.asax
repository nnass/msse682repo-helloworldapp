﻿<%@ Application Language="C#" %>
<%@ Import Namespace="WebSite3" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        AuthConfig.RegisterOpenAuth();
        Application.Lock();
        Application["SiteCounter"] = 0;
        Application.UnLock();
    }

    protected void Session_Start(object sender, EventArgs e)
    {
        Application.Lock();
        int counter = (int)Application["SiteCounter"];
        counter++;
        Application["SiteCounter"] = counter;
        Application.UnLock();
    }
    
    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

</script>
